"""
apoe.py

Excerpts from the base sequence for the APOE gene.
"""

## >gi|163644328|ref|NM_009696.3| Mus musculus apolipoprotein E (Apoe), mRNA

APOE_NORMAL = "GCAGGCGGAGATCTTCCAGGCCCGCCTCAAGGGCTGGTTCGAGCCAATAGTGGAAGACATGCATCGCCAGTGGGCAAACCTGATGGAGAAGATACAGGCCTCTGTGGCTACCAACCCCATCATCACCCCAGTGGCCCAGGAGAATCAATGAGTATCCTTCTCCTGTCCTGCAACAACATCCATATCCAGCCAGGTGGCCCTGTCTCAAGCACCTCTCTGGCCCTCTGGTGGCCCTTGCTTAATAAAGATTCTCCGAGCACATTCTGAGTCTCTGTGAGTGATTCCAAAAAAAAAAAAAAAAA"

APOE_BAD = "GCAGGCGGAGATCTTCCAGGCCCGCCTCAAGGGCTGGTTCGAGCCAATAGTGGAAGACATGCATCGCCAGTGGGCAAACCTGATGGAGAAGATACAGGCCTCTGTGCCTACCAACCCCATCATCACCCCAGTGGCCCAGGAGAATCAATGAGTATCCTTCTCCTGTCCTGCAACATCATCCATATCCAGCCAGGTGGCCCTGTCTCAAGCACCTCTCTGGCCCTCTGGTGGCCCTTGCTTAATAAAGATTCTCCGAGCACATTCTGAGTCTCTGTGAGTGATTCCAAAAAAAAAAAAAAAAA"
