"""
memoize.py

Implements a memoize decorator that can be used to memoize any function.
"""

def memoize(function):
    """ Memoization decorator for a function taking one or more arguments. """
    class MemoStore(dict):
        """
        Dictionary to keep track of results from previous calls to function.
        """
        def __getitem__(self, *key):
            """
            Returns the result of applying the function to the arguments, *key.  If the
            result is already stored in the dictionary, returns it directly.  If not,
            __missing__ will be called to evaluate the function on the inputs, store that
            result in the dictionary, and return it.
            """
            return dict.__getitem__(self, key)

        def __missing__(self, key):
            """
            Adds the result of applying function to *key arguments to the dictionary, and
            returns the result.
            """
            ret = self[key] = function(*key)
            return ret

    return MemoStore().__getitem__
